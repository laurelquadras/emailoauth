﻿using MailKit.Net.Pop3;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailOAuth
{
    class MailkitAppPassword
    {
        public async Task<List<EmailRequest>> mailbeeAppPasswordFlow(int maxCount = 10)
        {
            using (var emailClient = new Pop3Client())
            {
                emailClient.Connect("outlook.office365.com", 995);
                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                emailClient.Authenticate("flashtest2022@outlook.com", "vubpicxfcbwhquhy");

                List<EmailRequest> emails = new List<EmailRequest>();
                for (int i = 0; i < emailClient.Count && i < maxCount; i++)
                {
                    var message = await emailClient.GetMessageAsync(i);
                    var emailMessage = new EmailRequest
                    {
                        Body = !string.IsNullOrEmpty(message.HtmlBody) ? message.HtmlBody : message.TextBody,
                        Subject = message.Subject,
                        Attachments = message.Attachments.ToList().Count
                    };
                    emailMessage.EmailTo = message.To.First().Name;
                    emailMessage.EmailFrom = message.From.First().Name;
                    emails.Add(emailMessage);
                }
                return emails;
            }
        }
    }
}
