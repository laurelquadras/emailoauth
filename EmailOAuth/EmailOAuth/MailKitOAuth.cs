﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Util;
using Google.Apis.Util.Store;
using MailKit.Net.Pop3;
using MailKit.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmailOAuth
{
    class MailKitOAuth
    {
        public async Task<List<EmailRequest>> OAuth2Flow()
        {
            const string GMailAccount = "dstest@thinktankoc.com";

            var clientSecrets = new ClientSecrets
            {
                ClientId = "411999551820-2uojh3r6at3ec22d7d35t4rukr57vpuq.apps.googleusercontent.com",
                ClientSecret = "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"
            };

            var codeFlow = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
            {
                DataStore = new FileDataStore("CredentialCacheFolder", false),
                Scopes = new[] { "https://mail.google.com/" },
                ClientSecrets = clientSecrets
            });

            // Note: For a web app, you'll want to use AuthorizationCodeWebApp instead.
            var codeReceiver = new LocalServerCodeReceiver();
            var authCode = new AuthorizationCodeInstalledApp(codeFlow, codeReceiver);

            var credential = await authCode.AuthorizeAsync(GMailAccount, CancellationToken.None);

            if (credential.Token.IsExpired(SystemClock.Default))
                await credential.RefreshTokenAsync(CancellationToken.None);

            var oauth2 = new SaslMechanismOAuth2(credential.UserId, credential.Token.AccessToken);

            using (var client = new Pop3Client())
            {
                await client.ConnectAsync("pop.gmail.com", 995, SecureSocketOptions.SslOnConnect);
                await client.AuthenticateAsync(oauth2);
                List<EmailRequest> emails = new List<EmailRequest>();
                for (int i = 0; i < client.Count && i < 10; i++)
                {
                    var message = await client.GetMessageAsync(i);
                    var emailMessage = new EmailRequest
                    {
                        Body = !string.IsNullOrEmpty(message.HtmlBody) ? message.HtmlBody : message.TextBody,
                        Subject = message.Subject
                    };
                    emailMessage.EmailTo = message.To.First().Name;
                    emailMessage.EmailFrom = message.From.First().Name;
                    emails.Add(emailMessage);
                }
                return emails;
            }
        }
    }
}
