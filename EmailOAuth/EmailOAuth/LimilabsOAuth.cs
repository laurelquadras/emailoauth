﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Requests;
using Google.Apis.Auth.OAuth2.Responses;
using Limilabs.Client.Authentication.Google;
using Limilabs.Client.POP3;
using Limilabs.Mail;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EmailOAuth
{
    class LimilabsOAuth
    {
        public async Task limilabsOAuthPOC()
        {
            string userEmail = "dstest@thinktankoc.com";
            string clientID = "411999551820-2uojh3r6at3ec22d7d35t4rukr57vpuq.apps.googleusercontent.com";
            string clientSecret = "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";
            string redirectUri = "http://localhost/authorize/";

            var clientSecrets = new ClientSecrets
            {
                ClientId = clientID,
                ClientSecret = clientSecret
            };

            var credential = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
            {
                ClientSecrets = clientSecrets,
                Scopes = new[] { GoogleScope.Mail.Name }
            });

            AuthorizationCodeRequestUrl url = credential.CreateAuthorizationCodeRequest(redirectUri);
            System.Diagnostics.Process.Start("explorer.exe", $"\"{url.Build()}\"");

            Console.WriteLine("Give code");
            var authCode = Console.ReadLine();

            TokenResponse token = await credential.ExchangeCodeForTokenAsync("", authCode, redirectUri, CancellationToken.None);

            using Pop3 pop = new Pop3();
            pop.ConnectSSL("pop.gmail.com");
            pop.LoginOAUTH2(userEmail, token.AccessToken);
            MailBuilder builder = new MailBuilder();
            foreach (string uid in pop.GetAll())
            {
                IMail email = builder.CreateFromEml(
                  pop.GetMessageByUID(uid));

                Console.WriteLine(email.Subject);
                Console.WriteLine(email.Text);
            }
            pop.Close();
        }
    }
}
