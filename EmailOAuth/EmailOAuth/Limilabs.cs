﻿using Google.Apis.Auth.OAuth2;
using Limilabs.Client.POP3;
using Limilabs.Mail;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace EmailOAuth
{
    class Limilabs
    {
        public void LimilabsConnect()
        {
            string serviceAccountEmail = "flash-service-account-1@flash-service-account.iam.gserviceaccount.com";
            string userEmail = "dsflash@nehamohit.com";

            X509Certificate2 certificate = new X509Certificate2(@"C:\Users\lquadras\Downloads\flash-service-account-39b4233ac0c6.p12",
                "notasecret", X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
               new ServiceAccountCredential.Initializer(serviceAccountEmail)
               {
                   User = userEmail,
                   Scopes = new string[] { "https://mail.google.com/" }
               }.FromCertificate(certificate));

            if (credential.RequestAccessTokenAsync(CancellationToken.None).Result)
            {
                using (Pop3 pop = new Pop3())
                {
                    pop.ConnectSSL("pop.gmail.com");
                    pop.LoginOAUTH2(userEmail, credential.Token.AccessToken);
                    MailBuilder builder = new MailBuilder();
                    foreach (string uid in pop.GetAll())
                    {
                        IMail email = builder.CreateFromEml(
                          pop.GetMessageByUID(uid));

                        Console.WriteLine(email.Subject);
                        Console.WriteLine(email.Text);
                    }
                    pop.Close();
                }
            }
        }
    }
}
