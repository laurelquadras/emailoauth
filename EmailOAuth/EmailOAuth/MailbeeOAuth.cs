﻿using System;
using System.Collections.Specialized;
using MailBee;
using MailBee.ImapMail;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Oauth2.v2;
using Google.Apis.Services;
using MailBee.Pop3Mail;
using MailBee.Mime;

namespace EmailOAuth
{
	class MailbeeOAuth
	{
        public void MailbeeOAuthPOC()
        {
            string userEmail = "dsflashsprint@gmail.com";
            string[] scopes = new string[] { "https://mail.google.com/" };

            Console.WriteLine("Requesting authorization");
            UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                new ClientSecrets
                {
                    ClientId = "821728439396-7nrhnqrbnsc27fmhcsilthlk4uqqg63o.apps.googleusercontent.com",
                    ClientSecret = "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",
                },
                 scopes,
                 "user",
                 CancellationToken.None).Result;

            if (credential.Token.IsExpired(credential.Flow.Clock))
            {
                if (credential.RefreshTokenAsync(CancellationToken.None).Result)
                {
                    Console.WriteLine("The access token is now refreshed");
                }
            }
            string xoauthKey = OAuth2.GetXOAuthKeyStatic(userEmail, credential.Token.AccessToken);
            MailBee.Global.LicenseKey = "MN120-5C94944094AD9433944C56978BBF-8BE3";
            bool usePop = true; 

            if (usePop)
            {
                Pop3 pop = new Pop3();
                pop.Connect("pop.gmail.com");
                pop.Login(userEmail, xoauthKey, AuthenticationMethods.SaslOAuth2,
                    AuthenticationOptions.None, null);
                if (pop.IsLoggedIn && pop.InboxMessageCount > 0)
                {
                    MailMessage msg = pop.DownloadEntireMessage(pop.InboxMessageCount);

                    if (!string.IsNullOrEmpty(msg.BodyHtmlText))
                    {
                        Console.WriteLine(msg.BodyHtmlText);
                    }
                    else if (!string.IsNullOrEmpty(msg.BodyPlainText))
                    {
                        Console.WriteLine(msg.BodyPlainText);
                    }
                    else
                    {
                        Console.WriteLine("The message body is empty.");
                    }
                }
                pop.Disconnect();
            }
        }
    }
    
}
