﻿using Google.Apis.Auth.OAuth2;
using MailBee;
using MailBee.Mime;
using MailBee.Pop3Mail;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace EmailOAuth
{
	class Mailbee
	{
		public void MailbeeConnect()
		{
			string serviceAccountEmail = "flash-service-account-1@flash-service-account.iam.gserviceaccount.com";
			string userEmail = "dsflash@nehamohit.com";

			X509Certificate2 certificate = new X509Certificate2(@"C:\Users\lquadras\Downloads\flash-service-account-39b4233ac0c6.p12",
				"notasecret", X509KeyStorageFlags.Exportable);

			ServiceAccountCredential credential = new ServiceAccountCredential(
			   new ServiceAccountCredential.Initializer(serviceAccountEmail)
			   {
				   User = userEmail,
				   Scopes = new string[] { "https://mail.google.com/" }
			   }.FromCertificate(certificate));

			if (credential.RequestAccessTokenAsync(CancellationToken.None).Result)
			{
				string xoauthKey = OAuth2.GetXOAuthKeyStatic(userEmail, credential.Token.AccessToken);

				// Uncomment and set your key if you haven't specified it in app.config or Windows registry.
				MailBee.Global.LicenseKey = "MN120-5C94944094AD9433944C56978BBF-8BE3";

				bool useImap = true; // Set to false to use SMTP (send e-mail) instead of IMAP (check Inbox).

				if (useImap)
				{
					Pop3 pop = new Pop3();


					pop.Connect("pop.gmail.com");
					pop.Login(userEmail, xoauthKey, AuthenticationMethods.SaslOAuth2,
						AuthenticationOptions.None, null);
					if (pop.IsLoggedIn && pop.InboxMessageCount > 0)
					{
						MailMessage msg = pop.DownloadEntireMessage(pop.InboxMessageCount);

						if (!string.IsNullOrEmpty(msg.BodyHtmlText))
						{
							Console.WriteLine(msg.BodyHtmlText);
						}
						else if (!string.IsNullOrEmpty(msg.BodyPlainText))
						{
							Console.WriteLine(msg.BodyPlainText);
						}
						else
						{
							Console.WriteLine("The message body is empty.");
						}
					}
					pop.Disconnect();
				}
			}
		}
	}
}
