﻿using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Limilabs.Client.IMAP;
using Limilabs.Client.POP3;
using Limilabs.Mail;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Pop3;
using MailKit.Security;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmailOAuth
{
    class OutlookOAuthLimilabs
    {
        public async Task<string> GetAuthURI()
        {
            string clientId = "f02584d7-192d-47a1-8887-7ce7f732f593";
            string clientSecret = "rS.8Q~lR-KkHScjzrDPfaCPo6WUSrVf.VfrLucFY";

            var app = ConfidentialClientApplicationBuilder
                .Create(clientId)
                .WithAuthority(AadAuthorityAudience.PersonalMicrosoftAccount)
                .WithClientSecret(clientSecret)
                .WithAuthority("https://login.microsoftonline.com/common")
                .WithRedirectUri("https://emailsapi.local.dealersocket.com/api/v1/emails/receive-code")
                .Build();
            // This allows saving access/refresh tokens to some storage
            //TokenCacheHelper.EnableSerialization(app.UserTokenCache);

            var scopes = new string[]
            {
                "email",
                "offline_access",
                "https://outlook.office.com/POP.AccessAsUser.All",
            };

            string userName;
            string accessToken;
            string identifier = "00000000-0000-0000-1c3b-4a89159195ea.9188040d-6c67-4c5b-b112-36a304b66dad";

            var account = await app.GetAccountAsync(identifier);

            try
            {
                AuthenticationResult refresh = await app
                    .AcquireTokenSilent(scopes, account)
                    .WithForceRefresh(true)
                    .ExecuteAsync();

                userName = refresh.Account.Username;
                accessToken = refresh.AccessToken;
                return null;

            }
            catch (MsalUiRequiredException e)
            {
                Uri uri = await app
                    .GetAuthorizationRequestUrl(scopes)
                    .ExecuteAsync();

                return uri.AbsoluteUri;
            }
        }

        //Needs a controller layer, which Microsoft will call through it's redirect URI. The redirect URI has to call this method with the Auth Code
        public async Task AuthenticateWithCode(string code)
        {
            string userName;
            string accessToken;
            List<string> subject = new List<string>();
            string clientId = "f02584d7-192d-47a1-8887-7ce7f732f593";
            string clientSecret = "rS.8Q~lR-KkHScjzrDPfaCPo6WUSrVf.VfrLucFY";

            var app = ConfidentialClientApplicationBuilder
                .Create(clientId)
                .WithAuthority(AadAuthorityAudience.PersonalMicrosoftAccount)
                .WithClientSecret(clientSecret)
                .WithAuthority("https://login.microsoftonline.com/common")
                .WithRedirectUri("https://emailsapi.local.dealersocket.com/api/v1/emails/receive-code")
                .Build();

            var scopes = new string[]
            {
                "email",
                "offline_access",
                "https://outlook.office.com/POP.AccessAsUser.All",
            };

            AuthenticationResult result = await app
                    .AcquireTokenByAuthorizationCode(scopes, code)
                    .ExecuteAsync();

            string identifier = result.Account.HomeAccountId.Identifier;
            userName = result.Account.Username;
            accessToken = result.AccessToken;


            using (Pop3 pop = new Pop3())
            {
                pop.ConnectSSL("outlook.office365.com");
                pop.LoginOAUTH2(userName, accessToken);
                MailBuilder builder = new MailBuilder();
                foreach (string uid in pop.GetAll())
                {
                    IMail email = builder.CreateFromEml(
                      pop.GetMessageByUID(uid));

                    subject.Add(email.Subject);
                    Console.WriteLine(email.Subject);
                    Console.WriteLine(email.Text);
                }
                pop.Close();
            }
            Console.WriteLine(subject.ToArray());
        }
    }
}
