﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailOAuth
{
    class EmailRequest
    {
        public string EmailFrom { get; set; }

        public string EmailTo { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string? Cc { get; set; }

        public string? Bcc { get; set; }
        public int Attachments { get; set; }
    }
}
