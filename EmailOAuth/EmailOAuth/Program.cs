﻿using System;
using System.Threading.Tasks;

namespace EmailOAuth
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            OutlookOAuthLimilabs outlookOAuthLimilabs = new OutlookOAuthLimilabs();
            await outlookOAuthLimilabs.GetAuthURI();

            MailkitAppPassword mailkitAppPassword = new MailkitAppPassword();
            await mailkitAppPassword.mailbeeAppPasswordFlow();

            LimilabsOAuth limilabsOAuth = new LimilabsOAuth();
            await limilabsOAuth.limilabsOAuthPOC();

            MailbeeOAuth mailbeeFlashAccount = new MailbeeOAuth();
            mailbeeFlashAccount.MailbeeOAuthPOC();

            MailKitOAuth mailKitOAuth = new MailKitOAuth();
            await mailKitOAuth.OAuth2Flow();

            Mailbee mailbee = new Mailbee();
            mailbee.MailbeeConnect();

            Limilabs limilabs = new Limilabs();
            limilabs.LimilabsConnect();

        }
    }
}
